import requests

string = input('Enter the String : ')

data = {
    "data": string
}

try:
    requests.post('http://127.0.0.1:5000/get-string',data=data)
except requests.exceptions.ConnectionError:    
    print('\x1b[0;30;41m' + 'Error:' + '\x1b[0m REST-Service is not running!')
    sys.exit(-1)
