from flask import Flask, request
import requests

app = Flask(__name__)

# I AM SENDING IT FROM SEND.PY
# FUNCTION RECIEVING THE STRING, ADDING THE PHRASE AND SENDING IT TO ANOTHER URL
@app.route('/get-string', methods=['POST'])#url where you send the post request
def get_string():
    got = request.form.get('data')
    data = {
        'data': got+"_REST-Service"
    }
    requests.post('http://127.0.0.1:5000/send-string',data=data)#url where you want to send the post request
    return '200'

# THE URL WHERE YOU RECIEVE THE UPDATED STRING
@app.route('/send-string', methods=['POST'])
def send_string():
    print('url')
    print(request.form.get('data'))
    return '200'

if __name__ == "__main__":
    app.run(port=5000, debug=True)